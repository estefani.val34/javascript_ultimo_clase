/*  
@name= index.js
@author= Estefani Paredes Valera
@version= 1.0
@description= Controler of with index.html
@date = 24-11-2019
@params= none 
@return = none 
*/


/*  
@name= $(document).ready
@author= Estefani Paredes Valera
@version= 1.0
@description= Activate events when onload main page
@date = 24-11-2019
@params= none 
@return = none 
*/
var array_products = [];
var newProductType;


$(document).ready(function () {
    $("#div2").hide();
    $("#div1").show();
    $('#idNumProducts').val("");
    var IDpt = 0 + Math.floor(Math.random() * 2); //number between 0 and 2(NUMBER OF OPTIONS) : 0,1
    var optionselected = "DNA code";
    var numproduct;
    $("#pResult").html("YOU HAVE NOT INTRODUCED ANYTHING, ENTER A NUMBER !!!").hide();
    var validinputs = "";
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var today = new Date();
    var date = days[today.getDay()] + ", " + today.getDate() + " of " + months[today.getMonth()] + ' ' + today.getFullYear();

   

     /**
     * @name= $("#Product").on
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description= get the option selected when I click or change the selection
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $("#Product").on({
        click: function () {
            optionselected = $(this).val();
        }, change: function () {
            optionselected = $(this).val();
        }
    });


     /**
     * @name= $('#idNumProducts').on('input '
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Validate num and get the number of products
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $('#idNumProducts').on('input ', function () {
        var inputnum = $(this);
        numproduct = inputnum.val();

        if (numproduct == "") {
            $("#pResult").html("YOU HAVE NOT INTRODUCED ANYTHING, ENTER A NUMBER !!!");
            inputnum.removeClass("valid").addClass("invalid");
        } else {

            if (isNaN(numproduct)) {
                $("#pResult").html("The data introduce is not a number!!!");
                inputnum.removeClass("valid").addClass("invalid");
            } else {
                if (numproduct<=0) {
                    inputnum.removeClass("valid").addClass("invalid");
                    $("#pResult").html("The data introduce is not a positive number!!!");
                } else{     
                    inputnum.removeClass("invalid").addClass("valid");
                    $("#pResult").html("The data introduce is a number!!!");
                }
            }
        }
    });


     /**
     * @name=  $('#introduce').click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Create rows and validate when I click botton introduce
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $('#introduce').click(function (event) {
        var valid = $("#idNumProducts").hasClass("valid");
        if (valid) {
            $("#div2").show();
            $("#div1").hide();
            newProductType = new ProductType(IDpt, optionselected);

            $("#productType").text("Enter products for category " + newProductType.Name);
            $("#numberProducts").html(''); //so I don't repeat the table rows
            var table;
            for (var i = 0; i < numproduct; i++) {

                table = "<div style='width:95%; height:2px; background-color:teal; margin-left:20px; margin-top:3px;'></div>" +
                    "<div style='margin-left:25px;margin-top:5px;'>" +
                    "<table>" +
                    "<tr id='" + i + "'>" +
                    "<td style='width:230px;'>" +
                    "<input type='text' class='cl class_name" + i + "' placeholder='Input product name'>" +
                    "</td>" +
                    "<td style='width:230px;'>" +
                    "<input type='text' class='cc class_code" + i + "' placeholder='Input code'>" +
                    "</td>" +
                    "<td style='width:30px;'>" +
                    "<input class='class_check" + i + "' type='checkbox' >" +
                    "</td>" +
                    "</tr>" +
                    "</table>" +
                    "</div><br>";

                $("#numberProducts").append(table);
            }
        } else {
            $("#pResult").show();
            $('#idNumProducts').removeClass("valid").addClass("invalid");
        }
    });


     /**
     * @name=   $('#div2').on('input '
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Validate inputs 
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $('#div2').on('input ', function () {
        var re = /^[AGCTagct\n]+$/;
        validinputs = "valid";
        for (var i = 0; i < numproduct; i++) {
            var is_adn = re.test($(".class_code" + i).val());
            if (is_adn) {
                $(".class_code" + i).removeClass("invalid").addClass("valid");
            } else {
                validinputs = "invalid"
                $(".class_code" + i).removeClass("valid").addClass("invalid");
            }

            if ($(".class_name" + i).val() != "") {
                $(".class_name" + i).removeClass("invalid").addClass("valid");
            } else {
                validinputs = "invalid"
                $(".class_name" + i).removeClass("valid").addClass("invalid");
            }
        }
    });


     /**
     * @name=   $("#buttonData").click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Create objects type product and pushed array and validate when I click botton Introduce...
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $("#buttonData").click(function (event) {
        array_products = [];//clear pop up 
        if (validinputs == "invalid") {
            $("#correguirerrores").html("Fill in well please!!!");
        } else {
            if ($(".class_name" + 0).val() == "") {
                $("#correguirerrores").html("Fill in well please!!!");
            } else {

                $("#correguirerrores").html("");
                for (var i = 0; i < numproduct; i++) {

                    var IDp, namep, codep, testedp, entrydatep;
                    IDp = i;
                    namep = $(".class_name" + i).val();
                    codep = $(".class_code" + i).val();

                    checkbox = $(".class_check" + i).is(':checked');
                    if (checkbox == true) {
                        testedp = "YES"
                    } else {
                        testedp = "NO"
                    }
                    entrydatep = date;
                    var newProduct = new Product(IDp, newProductType.ID, namep, codep, testedp, entrydatep);
                    array_products.push(newProduct);
                }

                var decision = confirm("Do you really want to introduce this data?");
                if (decision) {
                    window.open("../Practica2.2/popUpWindows/popUpWindow.html", "blank", "width=400px, height=400px");

                }
            }
        }
    });


     /**
     * @name= $("#buttoncancel").click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description=  Return page main, qhen I clicl botton cancel
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $("#buttoncancel").click(function (event) {
        $("#div2").hide();
        $("#div1").show();
        $("#Product").val("DNA code");
        $("#idNumProducts").val("");
        $(".valid").removeClass("valid");
        $(".invalid").removeClass("invalid");
        $("#pResult").html("YOU HAVE NOT INTRODUCED ANYTHING, ENTER A NUMBER !!!").hide();
        array_products = [];
        newProductType = null;
        $("#correguirerrores").html("");
        optionselected = "DNA code";
    });



});



