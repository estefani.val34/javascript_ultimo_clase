/*  
@name= popUpWindow.html
@author= Estefani Paredes Valera
@version= 1.0
@description= Controler of popUpWindow.html
@date = 24-11-2019
@params= none 
@return = none 
*/


/*  
@name= $(document).ready
@author= Estefani Paredes Valera
@version= 1.0
@description= Activate events when onload popUp page
@date = 24-11-2019
@params= none 
@return = none 
*/
$(document).ready(function () {
    
    var wo=window.opener;
    var woarray=wo.array_products;
    var wopt=wo.newProductType.Name;
    $("#productType").html("Data introduced for product: "+wopt);
    $("#date").html(woarray[0].EntryDate);
    
    length=woarray.length;
    $("#totaltr").html("Total products introduced in the DataBase: "+length);
    var table;
        for (var i = 0; i <length ; i++) {
            table = "<div style='width:95%; height:2px; background-color:white; margin-left:20px; margin-top:3px;'></div>" +
                "<div style='margin-left:25px;margin-top:5px;'>" +
                "<table>" +
                "<tr >" +
                "<td style='width:230px;'>" +woarray[i].Name+"</td>" +
                "<td style='width:230px;'>" +woarray[i].Code.toUpperCase()+"</td>" +
                "<td style='width:30px;'>" +woarray[i].Tested+"</td>" +
                "</tr>" +
                "</table>" +
                "</div><br>";
            $("#tablepopup").append(table);
        }

        /*  
        @name= $("#buttonclose").click
        @author= Estefani Paredes Valera
        @version= 1.0
        @description= This method is close Window when the user click close Window button
        @date = 21-11-2019
        @params= none 
        @return = none 
        */
        $("#buttonclose").click(function () {
            window.close();
        });
        /*  
        @name= $("#buttonprint").click
        @author= Estefani Paredes Valera
        @version= 1.0
        @description= This method print the popUp Window when the user click Print button 
        @date = 21-11-2019
        @params= none 
        @return = none 
        */
        $("#buttonprint").click(function () {
            window.print();
        });

});