/*  
    @name=  Product.js
    @author= Estefani Paredes Valera
    @version= 1.0
    @description= This class create object Product
    @date = 21-11-2019
    @params= none 
    @return = none 
    */
   
class Product{
    //attributes
    _id;
    _idProductType;//la forEndKey de Product, Product type
    _name;
    _code;
    _tested;
    _entryDate;

    //constructor
    constructor(pID, pIdProductType, pName, pCode, pTested,pEntryDate){
        this._id=pID;
        this._idProductType=pIdProductType;
        this._name=pName;
        this._code=pCode;
        this._tested=pTested;
        this._entryDate=pEntryDate;
    }

    //getters i setters 

    get ID(){
        return this._ID;
    }

    set ID(pID){
        this._ID = pID;
    }

    get  IdProductType(){
        return this._idProductType;
    }

    set IdProductType(pIdProductType){
        this._idProductType = pIdProductType;
    }

    get Name(){
        return this._name;
    }

    set Name(pName){
        this._name = pName;
    }

    get Code(){
        return this._code;
    }

    set Code(pCode){
        this._code = pCode;
    }

    get Tested(){
        return this._tested;
    }

    set Tested(pTested){
        this._tested = pTested;
    }

    get EntryDate(){
        return this._entryDate;
    }

    set EntryDate(pEntryDate){
        this._entryDate = pEntryDate;
    }

}