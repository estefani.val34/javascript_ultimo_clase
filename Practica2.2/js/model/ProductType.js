/*  
    @name=  ProductType.js
    @author= Estefani Paredes Valera
    @version= 1.0
    @description= This class create object ProductType 
    @date = 21-11-2019
    @params= none 
    @return = none 
    */
   
class ProductType {
    //Prpperties definition 
    _ID; //num incremental 
    _name;
    
    //constructor
    constructor(pID, pName) {
        this._ID = pID;
        this._name = pName;
    }

    //getters i setters 
    get ID() {
        return this._ID;
    }

    set ID(pID) {
        this._ID = pID;
    }

    get Name() {
        return this._name;
    }

    set Name(pName) {
        this._name = pName;
    }
}