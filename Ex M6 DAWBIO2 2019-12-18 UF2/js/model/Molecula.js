/**
 *
 *
 * @class Molecula
 * Class representing a Molecula
 */

class Molecula {
    //properties
    _ID;
    _nametype;

    /**
     *Creates an instance of molecula.
    * @param {String} pID
    * @param {String} pNametype
    * @memberof MOlecula
    */
    constructor(pID, pNametype) {
        this._ID = pID;
        this._nametype = pNametype;
    }
    /**
     *
     * @return {String}: ID of the MOlecula
     * @memberof MOlecula
     */
    get ID() {
        return this._ID;
    }
    /**
    *
    * @return {String}: ID of the MOlecula
    * @memberof MOlecula
    */
    set ID(pID) {
        this._ID = pID;
    }
    /**
    *
    * @return {String}: Nametype of the MOlecula
    * @memberof MOlecula
    */
    get Nametype() {
        return this._nametype;
    }
    /**
    *
    * @return {String}: Nametype of the MOlecula
    * @memberof MOlecula
    */
    set Nametype(pNametype) {
        this._nametype = pNametype;
    }


}