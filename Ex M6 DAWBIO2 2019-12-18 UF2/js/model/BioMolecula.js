/**
 *
 *
 * @class BioMolecula
 * Class representing a BioMolecula
 */

class BioMolecula extends Molecula {
    //properties
    _numimages;
    /**
     *Creates an instance of bioMOlecula.
    * @param {String} pID
    * @param {String} pNametype
    * @param {String} pNumimages
    * @memberof bioMOlecula
    */
    constructor(pID, pNametype, pNumimages) {
        super(pID, pNametype);
        this._numimages = pNumimages;
    }
    /**
     *
     * @return {String}: Numimages of the bioMOlecula
     * @memberof bioMOlecula
     */
    get Numimages() {
        return this._numimages
    }
    /**
    *
    * @return {String}: Numimages of the bioMOlecula
    * @memberof bioMOlecula
    */
    set Numimages(pNumimages) {
        this._numimages = pNumimages;
    }

}