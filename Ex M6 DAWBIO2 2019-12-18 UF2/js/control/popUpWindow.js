/*  
@name= popUpWindow.html
@author= Estefani Paredes Valera
@version= 1.0
@description= Controler of popUpWindow.html
@date = 24-11-2019
@params= none 
@return = none 
*/


/*  
@name= $(document).ready
@author= Estefani Paredes Valera
@version= 1.0
@description= Activate events when onload popUp page
@date = 24-11-2019
@params= none 
@return = none 
*/

$(document).ready(function () {
    var wo = window.opener;
    var woamolecula = wo.newBiomolecula;
    var woID = wo.idImg;
    $("#moleculaType").html("The clicked image is: " + woamolecula.Nametype + "-" + woID);
    $("#div1popup").append('<img class="mol" id="' + woID + '" src="../images/' + woamolecula.Nametype + '/' + woamolecula.Nametype + '-' + woID + '.png" />');


    /*  
        @name= $("#buttonclose").click
        @author= Estefani Paredes Valera
        @version= 1.0
        @description= This method is close Window when the user click close Window button
        @date = 21-11-2019
        @params= none 
        @return = none 
        */
    $("#buttonclose").click(function () {
        window.close();
    });
    /*  
    @name= $("#buttonprint").click
    @author= Estefani Paredes Valera
    @version= 1.0
    @description= This method print the popUp Window when the user click Print button 
    @date = 21-11-2019
    @params= none 
    @return = none 
    */
    $("#buttonprint").click(function () {
        window.print();
    });

});