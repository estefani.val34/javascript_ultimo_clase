/*  
@name= index.js
@author= Estefani Paredes Valera
@version= 1.0
@description= Controler of with index.html
@date = 24-11-2019
@params= none 
@return = none 
*/


/*  
@name= $(document).ready
@author= Estefani Paredes Valera
@version= 1.0
@description= Activate events when onload main page
@date = 24-11-2019
@params= none 
@return = none 
*/
var newBiomolecula;
var idImg;
$(document).ready(function () {
    $("#div1").show();
    $("#div2").hide();
    $("#div3").hide();
    var molecula;
    var IDmol;

      /**
     * @name= $("#divlink a").click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description= get div2
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $("#divlink a").click(function () {
        event.preventDefault();
        $(this).attr("href", "http://www.google.com");
        $("#div1").show();
        $("#div2").show();
        $("#div3").hide();
    });
      /**
     * @name= $('#exec').click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description= get table when I click exec
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $('#exec').click(function () {
        molecula = $("#idTypeMolecula").val();
        IDmol = 0 + Math.floor(Math.random() * 2);

        var ID, molNametype, molnumimages;
        ID = IDmol;
        molNametype = molecula;
        if (molecula == "C2NOH7") {
            molnumimages = 8;
        }
        if (molecula == "bO2H6") {
            molnumimages = 4;
        }

        if (molecula == "C2NOH7" || molecula == "bO2H6") {

            newBiomolecula = new BioMolecula(ID, molNametype, molnumimages);

            $("#div3").show();
            $("#images").html('');
            var table;
            for (var i = 0; i < newBiomolecula.Numimages; i++) {

                table = "<div style='width:95%; height:2px; background-color:teal; margin-left:20px; margin-top:3px;'></div>" +
                    "<div style='margin-left:25px;margin-top:5px;'>" +
                    "<table>" +
                    "<tr  id='" + i + "'>" +
                    "<td style='width:230px;'>" +
                    '<img class="mol" id="' + i + '" src="images/' + newBiomolecula.Nametype + '/' + newBiomolecula.Nametype + '-' + i + '.png" />' +
                    "</td>" +
                    "<td style='width:230px;'>" +
                    '<a class="mola" href="#" id="' + i + '">' + newBiomolecula.Nametype + '-' + i + '</a>' +
                    "</td>" +
                    "</tr>" +
                    "</table>" +
                    "</div><br>";

                $("#images").append(table);
            }

        } else {
            alert("You aren't enter a valid molecula: C2NOH7 or bO2H6 without spaces");

        }

    });

      /**
     * @name= $('#images').on('click', '.mola', function ()
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description= get the popup 
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $('#images').on('click', '.mola', function () {
        console.log($(this));
        idImg = $(this).attr('id');
        console.log(idImg);

        var decision = confirm("Do you really want to introduce this data?");
        if (decision) {
            window.open("../Ex M6 DAWBIO2 2019-12-18 UF2/popUpWindows/popUpWindow.html", "blank", "width=400px, height=400px");
        }

    });

      /**
     * @name= $('#clear').click
     * @author= Estefani Paredes Valera
     * @version= 1.0
     * @description= get div1 when I click clear, clear page
     * @date = 24-11-2019
     * @params= none 
     * @return = none
     **/
    $('#clear').click(function () {
        $("#div1").show();
        $("#div2").hide();
        $("#div3").hide();
        molecula = "";
        $("#idTypeMolecula").val("");

    });

});