$(document).ready(function(){
   $("#div1").click(function(event){
       //alert("Hola");
       //$(this).html("<p>Hey</p>");
       //console.log("aqui");
       $(this).append("<h1>Añadido</h1>");//Este lo pone abajo del div
   }); 

   $("#myList").dblclick(function(event){ //doubleclick
    //console.log("a");
    //$("#myList li:first").clone().appendTo("#myList");
    $("#myList li:last").clone().appendTo("#myList");
    $("#div1").hide(3000, function(){ //Al hacer click en la lista, el div se oculta. Y depende cuanto pongas de numero se ocultara mas lento o mas rapido.
        $("#div1").show();
        //console.log("first");
    }); 
   });

   $("#div1 a").click(function(){
       event.preventDefault(); //No lleva al link. Lo para. Asi podremos ver que cambia el link en el href.
        $(this).attr("href", "http://www.google.com");
   });

   $("#div2").click(function(){ //Cunado clickemos al div2 se eliminara el div1.
    $("#div1").remove();
   });

   let sum = 0;
   let arr = [1,2,3,4,5];
   $.each(arr, function(index, value){
    sum += value;
   });

   /*$("#myList li").each(function(){
       let li = $(this);
       console.log(li); 
   });*/

   $("#div2").on("mouseenter", function(){
    console.log("in");
    $(this).css("background-color", "yellow");
    $("<div></div>").appendTo("body"); //Con esto creamos un div y lo añadimos al body
   });

   $("#div2").on("mouseout", function(){
    console.log("out");
    $(this).css("background-color", "brown")
   });
});