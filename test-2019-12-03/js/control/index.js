// JQuery code
$(document).ready(function () {
    $("#salary").val("345,23");



    $("#butSubmit").click(function (event) {
        //Stops the default behaviour of the component
        //that fired the event.
        //In this case, the submit stops.
        event.preventDefault();

        var DNI, empName, surname, phone, birthdate, salary;
        DNI = $("#DNI").val();
        empName = $("#empName").val();
        surname = $("#surname").val();
        phone = $("#phone").val();
        birthdate = $("#birthdate").val();
        salary = $("#salary").val();

        var newEmployee =
            new Employee(DNI, empName, surname, phone, birthdate,
                salary);

        console.log(newEmployee);
        console.log(newEmployee.DNI);

    });

    $("#myFS").on("blur", ".currency", function () {
        $(".currency").formatCurrency(
            {
                region: 'es-ES',
                symbol: '€',
                positiveFormat: '%n %s',
                negativeFormat: '-%n %s',
                decimalSymbol: ',',
                digitGroupSymbol: '.',
                groupDigits: true
            }
        );
    });
    $("#myFS .currency").trigger("blur");// para que salga al recargar la pagina lo del euro
});


/**
 * GENERAR LA DOCUMENTACION DE JSDOC:
 * sudo jsdoc js/model/*.js-> GENERA LA CARPETA OUT
 * generar node_modules : npm install jsdoc
 */